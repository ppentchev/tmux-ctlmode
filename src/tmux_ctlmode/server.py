"""Start a server, process events."""

import dataclasses
import enum
import os
import pathlib
import re
import shlex
import sys

from typing import Callable, Dict, List, NamedTuple, Optional, Union


RE_ENCODED = re.compile(r" ^ (?P<code> [0-9]{3} ) (?P<rest> .* ) $ ", re.X)


PathStr = Union[str, os.PathLike]
PathList = List[PathStr]


def cmd_str(cmd: PathList) -> str:
    """Turn a command into a string."""
    str_gen = (str(word) for word in cmd)
    if sys.version_info > (3, 8):
        return shlex.join(str_gen)
    return " ".join(str_gen)


def decode_bytes(chars: str) -> bytes:
    """Decode the tmux output into bytes."""
    res = b""
    while True:
        literal, sep, rest = chars.partition("\\")
        res += literal.encode("UTF-8")
        if not sep:
            break

        enc = RE_ENCODED.match(rest)
        if not enc:
            raise ServerError(f"Invalid escape sequence in {chars!r}")
        res += bytes([int(enc.group("code"), 8)])
        chars = enc.group("rest")

    return res


class State(str, enum.Enum):
    """The state of the server."""

    COMMAND_OUTPUT = "gathering the output of a command"
    INITIALIZING = "initializing"
    STARTING = "starting"
    RUNNING = "running"
    ENDED = "ended"


@dataclasses.dataclass
class Pane:
    """A window pane."""

    pane_id: str
    rcode: Optional[int]


@dataclasses.dataclass
class Window:
    """A window containing panes and stuff."""

    win_id: str
    name: str
    panes: Dict[str, Pane]


@dataclasses.dataclass
class Session:
    """A session containing windows and stuff."""

    sess_id: str
    name: str
    windows: Dict[str, Window]


class ServerError(Exception):
    """An error that occurred while handling the tmux communication."""


class WindowID(NamedTuple):
    """Enough information to identify a window."""

    sess_id: str
    win_id: str


class PaneID(NamedTuple):
    """Enough information to identify a window pane."""

    sess_id: str
    win_id: str
    pane_id: str


@dataclasses.dataclass
class Event:
    """Some kind of event."""

    evt_type: str


@dataclasses.dataclass
class ProgramStartedEvent(Event):
    """A program was started within a tmux pane."""

    pane_id: str


@dataclasses.dataclass
class ProgramEndedEvent(Event):
    """A program ended within a tmux pane."""

    pane_id: str
    rcode: int


@dataclasses.dataclass
class OutputEvent(Event):
    """Text was sent to a window."""

    sess_id: str
    win_id: str
    pane_id: str
    data: bytes


@dataclasses.dataclass
class SendEvent(Event):
    """Data to send to the tmux process."""

    data: bytes


@dataclasses.dataclass
class Command:
    """An issued command that we are expecting a response to."""

    command: str
    output: List[str]
    callback: List[Callable[["Command"], List[Event]]]


@dataclasses.dataclass
class Server:
    """A tmux server controlling sessions, windows, panes, etc."""

    # pylint: disable=too-many-instance-attributes

    config_file: pathlib.Path
    socket_path: pathlib.Path

    # Mostly internal fields...

    state: State = dataclasses.field(init=False, default=State.INITIALIZING)
    command_state: Optional[State] = dataclasses.field(
        init=False, default=None
    )

    expected_sess_name: Optional[str] = dataclasses.field(
        init=False, default=None
    )

    commands: List[Command] = dataclasses.field(
        init=False, default_factory=list
    )

    sessions: Dict[str, Session] = dataclasses.field(
        init=False, default_factory=dict
    )
    current_session: Optional[str] = dataclasses.field(
        init=False, default=None
    )
    windows: Dict[str, WindowID] = dataclasses.field(
        init=False, default_factory=dict
    )
    panes: Dict[str, PaneID] = dataclasses.field(
        init=False, default_factory=dict
    )

    @staticmethod
    def get_config_file_contents() -> str:
        """Return the contents of the config file to write out."""
        return (
            "set-option -g remain-on-exit on\n"
            'set-option -g pane-died "'
            "display-message -p '"
            "!tmuxctl-pane-died #{window_id} #{pane_id} "
            "#{pane_dead} #{pane_dead_status}'\"\n"
        )

    def get_start_command(self, sess_name: str, cmd: PathList) -> PathList:
        """Get the command needed to start the server."""
        if self.state != State.INITIALIZING:
            raise ServerError(
                "get_start_command() invoked in state {self.state}"
            )
        if self.commands:
            raise ServerError("Did not expected commands: {self.commands!r}")
        if self.sessions:
            raise ServerError("Did not expected sessions: {self.sessions!r}")

        self.expected_sess_name = sess_name
        self.state = State.STARTING
        self.commands.append(
            Command(
                command="new-session",
                output=[],
                callback=[self._create_first_session],
            )
        )

        res: PathList = [
            "tmux",
            "-f",
            self.config_file,
            "-C",
            "-S",
            self.socket_path,
            "--",
            "new-session",
            "-s",
            sess_name,
            "-P",
            "-F",
            "#{session_id} #{window_id} #{pane_id}",
            "--",
        ]
        return res + cmd

    def get_check_running_command(self) -> PathList:
        """Check whether the server is still running."""
        return ["tmux", "-S", self.socket_path, "--", "has-session"]

    def get_kill_server_command(self) -> PathList:
        """Kill any server running."""
        return ["tmux", "-S", self.socket_path, "--", "kill-server"]

    def handle_lines(self, lines: Optional[str]) -> List[Event]:
        """Handle a command from the tmux client."""
        handlers = {
            "%begin": self._cmd_begin,
            "%end": self._cmd_end,
            "%exit": self._cmd_exit,
            "%pane-mode-changed": self._cmd_pane_mode_changed,
            "%window-add": self._cmd_window_add,
            "%window-renamed": self._cmd_window_renamed,
            "%sessions-changed": self._cmd_sessions_changed,
            "%session-changed": self._cmd_session_changed,
            "%output": self._cmd_output,
            "!tmuxctl-pane-died": self._cmd_tmux_pane_died,
        }

        if lines is None:
            if self.state != State.RUNNING:
                raise ServerError(f"EOF from tmux in state {self.state}")

            for sess in self.sessions.values():
                for win in sess.windows.values():
                    for pane in win.panes.values():
                        if pane.rcode is None:
                            raise ServerError(
                                f"EOF from tmux with pane {pane.pane_id} "
                                f"still alive"
                            )

            self.state = State.ENDED
            return []

        if self.state not in (
            State.STARTING,
            State.RUNNING,
            State.COMMAND_OUTPUT,
        ):
            raise ServerError(f"handle_lines({lines!r}) in state {self.state}")

        res = []
        for line in lines.splitlines():
            if self.state == State.COMMAND_OUTPUT:
                if not line.startswith("%end "):
                    self.commands[0].output.append(line)
                    continue

            word, _, rest = line.partition(" ")
            handler = handlers.get(word)
            if handler is not None:
                res.extend(handler(word, rest))
            else:
                raise NotImplementedError(
                    f"Unexpected tmux response: {line!r}"
                )

        return res

    def _cmd_begin(self, word: str, rest: str) -> List[Event]:
        """Start gathering a command's output."""
        if not self.commands:
            raise ServerError(f"'{word} {rest}' without a pending command")
        if self.command_state is not None:
            raise ServerError(
                f"{word!r} with command_state '{self.command_state}'"
            )

        self.command_state = self.state
        self.state = State.COMMAND_OUTPUT
        return []

    def _cmd_end(self, word: str, rest: str) -> List[Event]:
        """Done with a command's output."""
        if self.state != State.COMMAND_OUTPUT:
            raise ServerError(f"'{word} {rest}' in server state {self.state}")
        assert self.command_state is not None

        self.state = self.command_state
        self.command_state = None
        cmd = self.commands.pop(0)
        return cmd.callback[0](cmd)

    def _create_first_session(self, cmd: Command) -> List[Event]:
        """Got a %begin/%end pair for the initial 'new-session' command."""
        if self.state != State.STARTING:
            raise ServerError(f"{cmd!r} too late")
        if self.sessions:
            raise ServerError(f"Did not expect sessions: {self.sessions!r}")
        assert self.expected_sess_name is not None

        lines = cmd.output
        if len(lines) != 1:
            raise ServerError(
                f"Expected a single line for 'new-session': {lines!r}"
            )
        parts = lines[0].split(" ")
        if (
            len(parts) != 3
            or not parts[0].startswith("$")
            or not parts[1].startswith("@")
            or not parts[2].startswith("%")
        ):
            raise ServerError(
                f"Unexpected 'new-session' response: {lines[0]!r}"
            )
        sess_id, win_id, pane_id = parts

        self.state = State.RUNNING
        self.sessions[sess_id] = Session(
            sess_id=sess_id, name=self.expected_sess_name, windows={}
        )
        self._add_window(sess_id, Window(win_id=win_id, name="", panes={}))
        self._add_pane(sess_id, win_id, Pane(pane_id=pane_id, rcode=None))

        self.current_session = sess_id

        return [
            ProgramStartedEvent(evt_type="program-started", pane_id=pane_id)
        ]

    def _add_window(self, sess_id: str, win: Window) -> None:
        """Add a window to the appropriate lists and indices."""
        sess = self.sessions.get(sess_id)
        if sess is None:
            raise ServerError(
                f"_add_window({sess_id!r}, {win!r}): unknown session"
            )
        if win.win_id in sess.windows or win.win_id in self.windows:
            raise ServerError(
                f"_add_window({sess_id!r}, {win!r}): duplicate window id"
            )
        sess.windows[win.win_id] = win
        self.windows[win.win_id] = WindowID(sess_id, win.win_id)

    def _add_pane(self, sess_id: str, win_id: str, pane: Pane) -> None:
        """Add a window pane to the appropriate lists and indices."""
        sess = self.sessions.get(sess_id)
        if sess is None:
            raise ServerError(
                f"_add_pane({sess_id!r}, {win_id!r}, {pane!r}): "
                f"unknown session"
            )
        win = sess.windows.get(win_id)
        if win is None:
            raise ServerError(
                f"_add_pane({sess_id!r}, {win_id!r}, {pane!r}): unknown window"
            )
        if pane.pane_id in win.panes or pane.pane_id in self.panes:
            raise ServerError(
                f"_add_pane({sess_id!r}, {win_id!r}, {pane!r}): "
                f"duplicate pane id"
            )
        win.panes[pane.pane_id] = pane
        self.panes[pane.pane_id] = PaneID(sess_id, win_id, pane.pane_id)

    def _cmd_window_add(self, word: str, rest: str) -> List[Event]:
        """Add a window to the currently-active session."""
        assert self.current_session is not None
        sess = self.sessions.get(self.current_session)
        if sess is None:
            raise ServerError(f"'{word} {rest}' without a session")
        if not rest:
            raise ServerError(f"'{word}' without parameters")

        win_id, _, rest = rest.partition(" ")
        if win_id in sess.windows:
            if (
                self.expected_sess_name is None
                or sess.name != self.expected_sess_name
            ):
                raise ServerError(
                    f"Duplicate window {win_id!r} in session {sess.sess_id!r}"
                )
        else:
            self._add_window(
                self.current_session,
                Window(win_id=win_id, name="", panes={}),
            )
        return []

    def _cmd_sessions_changed(self, _word: str, _rest: str) -> List[Event]:
        """Reread the list of sessions."""
        # pylint: disable=no-self-use
        return []

    def _cmd_session_changed(self, word: str, rest: str) -> List[Event]:
        """For the present, only record the first session."""
        if not rest:
            raise ServerError(f"'{word}' without parameters")
        sess_id, sep, sess_name = rest.partition(" ")
        if not sep or not sess_name:
            raise ServerError(f"'{word}' with too few parameters")
        if (
            self.current_session == sess_id
            and self.expected_sess_name is not None
        ):
            if sess_name != self.expected_sess_name:
                raise ServerError(
                    f"Expected session name '{self.expected_sess_name}', "
                    f"got '{sess_name}' for '{sess_id}'"
                )
            self.expected_sess_name = None

        self.get_session(sess_id).name = sess_name
        self.current_session = sess_id
        return []

    def get_session(self, sess_id: str) -> Session:
        """Make sure we know about this session."""
        try:
            return self.sessions[sess_id]
        except KeyError as err:
            raise ServerError(f"Unknown session id {sess_id!r}") from err

    def get_window(self, win_id: str) -> Window:
        """Make sure we know about this window."""
        try:
            path = self.windows[win_id]
            return self.sessions[path.sess_id].windows[path.win_id]
        except KeyError as err:
            raise ServerError(f"Unknown window id {win_id!r}") from err

    def get_pane(self, pane_id: str) -> Pane:
        """Make sure we know about this pane."""
        try:
            path = self.panes[pane_id]
            return (
                self.sessions[path.sess_id]
                .windows[path.win_id]
                .panes[path.pane_id]
            )
        except KeyError as err:
            raise ServerError(f"Unknown pane id {pane_id!r}") from err

    def _cmd_output(self, word: str, rest: str) -> List[Event]:
        """Whee!"""
        pane_id, sep, text = rest.partition(" ")
        if not sep or not text:
            raise ServerError(f"Too few arguments to {word!r}: {rest!r}")

        data = decode_bytes(text)
        path = self.panes[pane_id]
        return [
            OutputEvent(
                evt_type="pane-output",
                sess_id=path.sess_id,
                win_id=path.win_id,
                pane_id=path.pane_id,
                data=data,
            )
        ]

    def _cmd_window_renamed(self, word: str, rest: str) -> List[Event]:
        """Update a window's name."""
        win_id, sep, win_name = rest.partition(" ")
        if not sep or not win_name:
            raise ServerError(f"Too few parameters to {word!r}: {rest!r}")

        self.get_window(win_id).name = win_name
        return []

    def send_keys_literal(
        self, pane_id: str, text: str, callback: Callable[[], List[Event]]
    ) -> List[Event]:
        """Send some text to a pane."""

        def call_back(_cmd: Command) -> List[Event]:
            """Call back into the, well, callback provided."""
            return callback()

        self.get_pane(pane_id)
        self.commands.append(
            Command(command="send-keys", output=[], callback=[call_back])
        )
        encoded = " ".join(
            "{0:02X}".format(value) for value in text.encode("UTF-8")
        )

        return [
            SendEvent(
                evt_type="send-keys",
                data=f"send-keys -t {pane_id} -H {encoded}\n".encode("UTF-8"),
            )
        ]

    def send_key_name(
        self, pane_id: str, key: str, callback: Callable[[], List[Event]]
    ) -> List[Event]:
        """Send a single key name to a pane."""

        def call_back(_cmd: Command) -> List[Event]:
            """Call back into the, well, callback provided."""
            return callback()

        self.get_pane(pane_id)
        self.commands.append(
            Command(command="send-keys", output=[], callback=[call_back])
        )

        return [
            SendEvent(
                evt_type="send-keys",
                data=f"send-keys -t {pane_id} {key}\n".encode("UTF-8"),
            )
        ]

    def capture_pane(
        self, pane_id: str, callback: Callable[[List[str]], List[Event]]
    ) -> List[Event]:
        """Capture the contents of a window pane."""

        def call_back(cmd: Command) -> List[Event]:
            """Call back into the, well, callback provided."""
            return callback(cmd.output)

        self.get_pane(pane_id)
        self.commands.append(
            Command(command="capture-pane", output=[], callback=[call_back])
        )

        return [
            SendEvent(
                evt_type="capture-pane",
                data=f"capture-pane -p -t {pane_id}\n".encode("UTF-8"),
            )
        ]

    def _cmd_pane_mode_changed(self, _word: str, _rest: str) -> List[Event]:
        """Nothing to do, really."""
        # pylint: disable=no-self-use
        return []

    def _cmd_tmux_pane_died(self, word: str, rest: str) -> List[Event]:
        """Mark the pane as being reaped, kill it."""
        parts = rest.split(" ")
        if len(parts) < 4:
            raise ServerError(f"Too few parameters to {word!r}: {rest!r}")
        _, pane_id, pane_dead, rcode_str = parts[:4]
        if not pane_dead:
            return []
        if word == "!tmuxctl-pane-list" and rcode_str == "":
            rcode = -1
        else:
            try:
                rcode = int(rcode_str, 10)
            except ValueError as err:
                raise ServerError(
                    f"Invalid pane exit code {rcode_str!r}"
                ) from err

        pane = self.get_pane(pane_id)
        if pane.rcode is not None:
            # Check if it is the same? Nah...
            return []
        pane.rcode = rcode

        def callback(_cmd: Command) -> List[Event]:
            """Report the program being done."""
            return [
                ProgramEndedEvent(
                    evt_type="program-end", pane_id=pane_id, rcode=rcode
                )
            ]

        self.commands.append(
            Command(command="kill-pane", output=[], callback=[callback])
        )
        return [
            SendEvent(
                evt_type="kill-pane",
                data=f"kill-pane -t {pane_id}\n".encode("UTF-8"),
            )
        ]

    def _cmd_exit(self, _word: str, _rest: str) -> List[Event]:
        """Do nothing, really. Should be handled by null input."""
        # pylint: disable=no-self-use
        return []

    def list_all_known_panes(self) -> List[Event]:
        """Go through all the windows, run a 'list panes' command."""

        def cb_window(cmd: Command) -> List[Event]:
            """Process a single window's panes."""
            res: List[Event] = []
            for line in cmd.output:
                res.extend(
                    self._cmd_tmux_pane_died("!tmuxctl-pane-list", line)
                )

            return res

        res: List[Event] = []
        for sess in self.sessions.values():
            for window in sess.windows.values():
                self.commands.append(
                    Command(
                        command="list-panes", output=[], callback=[cb_window]
                    )
                )
                res.append(
                    SendEvent(
                        evt_type="list-panes",
                        data=(
                            "list-panes -F '"
                            "#{window_id} #{pane_id} "
                            "#{pane_dead} #{pane_dead_status}"
                            "' -t " + window.win_id + "\n"
                        ).encode("UTF-8"),
                    )
                )

        return res
