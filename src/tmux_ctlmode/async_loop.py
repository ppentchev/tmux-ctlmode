"""An asyncio wrapper for the tmux_ctlmode.server.Server class."""

from __future__ import annotations

import asyncio
import dataclasses
import pathlib

from asyncio import subprocess

from typing import (
    Any,
    Callable,
    Coroutine,
    Dict,
    List,
    Optional,
    Type,
    TypeVar,
)

import cfg_diag
import decode_acc.accumulator as decode_acc
import decode_acc.newlines as decode_lines

from . import server as tserver


@dataclasses.dataclass(frozen=True)
class LoopEvent:
    """An internal event for the AsyncLoop."""

    esrc: str


@dataclasses.dataclass(frozen=True)
class OutputEvent(LoopEvent):
    """The tmux server produced output, feed it to the server object."""

    data: bytes


@dataclasses.dataclass(frozen=True)
class OutputDoneEvent(LoopEvent):
    """The tmux server will not produce any more output."""


@dataclasses.dataclass(frozen=True)
class ErrorOutputEvent(LoopEvent):
    """The tmux server produced output on stderr, break and run."""

    char: bytes


@dataclasses.dataclass(frozen=True)
class ProgramStartedEvent(LoopEvent):
    """The program was started."""

    event: tserver.ProgramStartedEvent
    sess_id: str
    win_id: str
    pane_id: str


@dataclasses.dataclass(frozen=True)
class StopLoopEvent(LoopEvent):
    """Somebody told us to stop the loop."""


@dataclasses.dataclass(frozen=True)
class SendLiteralEvent(LoopEvent):
    """Send a literal string to a pane."""

    pane_id: str
    text: str
    callback: List[Callable[[], List[tserver.Event]]]


@dataclasses.dataclass(frozen=True)
class SendKeyNameEvent(LoopEvent):
    """Send a literal string to a pane."""

    pane_id: str
    key: str
    callback: List[Callable[[], List[tserver.Event]]]


@dataclasses.dataclass(frozen=True)
class CapturePaneEvent(LoopEvent):
    """Capture the contents of a pane."""

    pane_id: str
    callback: List[Callable[[List[str]], List[tserver.Event]]]


@dataclasses.dataclass(frozen=True)
class SchedulePaneListEvent(LoopEvent):
    """Schedule a periodic "list all panes" event."""


@dataclasses.dataclass
class SrvCapturePaneEvent(tserver.Event):
    """Captured the output of a pane."""

    output: List[str]
    queue: "asyncio.Queue[List[str]]"


@dataclasses.dataclass
class PaneData:
    """Queues and events related to a single pane."""

    output: "asyncio.Queue[bytes]" = dataclasses.field(
        default_factory=asyncio.Queue
    )
    rcode: Optional[int] = dataclasses.field(default=None)
    rcode_set: asyncio.Event = dataclasses.field(default_factory=asyncio.Event)


TLoopEvent = TypeVar("TLoopEvent", bound=LoopEvent)

T = TypeVar("T")  # pylint: disable=invalid-name
TRes = TypeVar("TRes")


def get_handler(
    handlers: Dict[Type[T], Callable[[T], TRes]], evt: T
) -> Optional[Callable[[T], TRes]]:
    """Pick the best handler for the event."""
    found = [item for item in handlers.items() if isinstance(evt, item[0])]
    if not found:
        return None
    if len(found) > 1:
        raise NotImplementedError(f"FIXME: find the top class: {found!r}")
    return found[0][1]


@dataclasses.dataclass
class AsyncLoop(cfg_diag.ConfigDiagUnfrozen):
    """A simple loop around AsyncServer handling some of the details."""

    # pylint: disable=too-many-instance-attributes

    config_file: pathlib.Path
    socket_path: pathlib.Path
    utf8_env: Dict[str, str]

    msgq: Optional["asyncio.Queue[LoopEvent]"] = dataclasses.field(
        default=None
    )
    user_handlers: Dict[
        Type[LoopEvent],
        Callable[[LoopEvent], Coroutine[Any, Any, List[tserver.Event]]],
    ] = dataclasses.field(default_factory=dict)
    acc: decode_acc.DecodeAccumulator = dataclasses.field(
        default_factory=decode_acc.DecodeAccumulator
    )
    acc_lines: decode_lines.TextSplitter = dataclasses.field(
        default_factory=decode_lines.UniversalNewlines
    )
    loop_lock: asyncio.Lock = dataclasses.field(default_factory=asyncio.Lock)
    session_started: Dict[
        str, "asyncio.Queue[ProgramStartedEvent]"
    ] = dataclasses.field(default_factory=dict)
    pane_data: Dict[str, PaneData] = dataclasses.field(default_factory=dict)
    pane_data_lock: asyncio.Lock = dataclasses.field(
        default_factory=asyncio.Lock
    )

    srv: tserver.Server = dataclasses.field(init=False)
    proc: Optional[
        subprocess.Process  # pylint: disable=no-member
    ] = dataclasses.field(init=False)

    srv_handlers: Dict[
        Type[tserver.Event],
        Callable[[tserver.Event], Coroutine[Any, Any, List[LoopEvent]]],
    ] = dataclasses.field(init=False)
    own_handlers: Dict[
        Type[LoopEvent],
        Callable[[LoopEvent], Coroutine[Any, Any, List[tserver.Event]]],
    ] = dataclasses.field(init=False)

    def __post_init__(self) -> None:
        """Initialize our internal fields."""
        self.srv = tserver.Server(
            config_file=self.config_file, socket_path=self.socket_path
        )
        self.proc = None

        self.srv_handlers = {
            tserver.OutputEvent: self._handle_pane_output,
            tserver.ProgramEndedEvent: self._program_ended,
            tserver.ProgramStartedEvent: self._program_started,
            tserver.SendEvent: self._send_command,
            SrvCapturePaneEvent: self._pane_captured,
        }
        self.own_handlers = {
            CapturePaneEvent: self._capture_pane,
            OutputEvent: self._handle_output,
            OutputDoneEvent: self._handle_output_done,
            SchedulePaneListEvent: self._schedule_pane_list,
            SendLiteralEvent: self._send_keys_literal,
            SendKeyNameEvent: self._send_key_name,
        }

    async def register_event_handler(
        self,
        etype: Type[TLoopEvent],
        callback: Callable[
            [TLoopEvent], Coroutine[Any, Any, List[tserver.Event]]
        ],
    ) -> None:
        """Register a handler for a type of async loop events."""
        self.user_handlers[etype] = callback  # type: ignore

    async def _handle_output(self, evt: LoopEvent) -> List[tserver.Event]:
        """Process tmux output, return the server events generated."""
        assert isinstance(evt, OutputEvent)
        self.acc, decoded = self.acc.add(evt.data).pop_decoded()
        if not decoded:
            return []
        self.acc_lines, lines = self.acc_lines.add_string(decoded).pop_lines()
        if not lines:
            return []
        self.diag(f"AsyncLoop._handle_output: {lines!r}")
        return self.srv.handle_lines("\n".join(lines))

    async def _handle_output_done(
        self, _evt: LoopEvent
    ) -> List[tserver.Event]:
        """No more tmux output."""
        self.diag("AsyncLoop: no more output from tmux")
        return self.srv.handle_lines(None)

    async def _ensure_pane_data(self, pane_id: str) -> None:
        """Make sure there is a pane data structure."""
        if pane_id in self.pane_data:
            return

        async with self.pane_data_lock:
            if pane_id in self.pane_data:
                return

            self.pane_data[pane_id] = PaneData()

    async def _program_started(self, evt: tserver.Event) -> List[LoopEvent]:
        """Convert a server's program started event into our own."""
        assert isinstance(evt, tserver.ProgramStartedEvent)
        self.diag(f"AsyncLoop._program_started: pane id {evt.pane_id}")
        path = self.srv.panes[evt.pane_id]
        self.diag(f"AsyncLoop._program_started: path {path!r}")
        await self._ensure_pane_data(path.pane_id)
        sess = self.srv.sessions[path.sess_id]
        self.diag(f"AsyncLoop._program_started: session name {sess.name!r}")
        await self.session_started[sess.name].put(
            ProgramStartedEvent(
                esrc=evt.evt_type,
                event=evt,
                sess_id=path.sess_id,
                win_id=path.win_id,
                pane_id=path.pane_id,
            )
        )
        return []

    async def program_started(self, sess_name: str) -> ProgramStartedEvent:
        """Wait for the "program started" event."""
        self.diag(
            f"AsyncLoop.program_started: waiting for session {sess_name!r}"
        )
        evt = await self.session_started[sess_name].get()
        self.diag(f"AsyncLoop.program_started: got event {evt!r}")
        return evt

    async def _feed_server(self) -> None:
        """Feed the process's standard output stream to the server."""
        proc = self.proc
        assert proc is not None and proc.stdout is not None
        while True:
            self.diag("AsyncLoop.feed_server: reading tmux stdout")
            line = await proc.stdout.readline()
            self.diag(f"AsyncLoop.feed_server: read stdout: {line!r}")
            if not line:
                try:
                    await self.msgq.put(  # type: ignore
                        OutputDoneEvent(esrc="feed_server")
                    )
                except AttributeError:
                    self.diag("AsyncLoop.feed_server: no one to tell")
                break

            try:
                await self.msgq.put(  # type: ignore
                    OutputEvent(esrc="feed_server", data=line)
                )
            except AttributeError:
                self.diag("AsyncLoop.feed_server: no one to tell about it")
                break

    async def _break_on_stderr(self) -> None:
        """Fail if the process writes anything to its stderr stream."""
        proc = self.proc
        assert proc is not None and proc.stderr is not None
        try:
            evt: LoopEvent = ErrorOutputEvent(
                esrc="break_on_stderr",
                char=await proc.stderr.readexactly(1),
            )
        except asyncio.IncompleteReadError:
            self.diag("AsyncLoop.break_on_stderr: done")
            return

        self.diag(f"AsyncLoop.break_on_stderr: {evt!r}")
        try:
            await self.msgq.put(evt)  # type: ignore
        except AttributeError:
            self.diag("AsyncLoop.break_on_stderr: no one to tell about it")

    async def _process_server_event(
        self, evt: tserver.Event
    ) -> List[LoopEvent]:
        """Process some reaction events from the tmux server."""
        handler = get_handler(self.srv_handlers, evt)
        if handler is None:
            raise NotImplementedError(f"FIXME: handle {evt!r}")
        return await handler(evt)

    async def _start_server(
        self, sess_name: str, cmd: tserver.PathList
    ) -> subprocess.Process:  # pylint: disable=no-member
        """Start the tmux session."""
        self.diag(
            f"AsyncLoop._start_server for session {sess_name!r}, "
            f"command {tserver.cmd_str(cmd)}"
        )
        scmd = self.srv.get_start_command(sess_name, cmd)
        self.diag(f"AsyncLoop._start_server: running {tserver.cmd_str(scmd)}")
        proc = await asyncio.create_subprocess_exec(
            *(str(word) for word in scmd),
            bufsize=0,
            env=self.utf8_env,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        self.diag(f"AsyncLoop._start_server: started process {proc.pid}")
        return proc

    async def _kill_server(self) -> None:
        """Kill the tmux server process."""
        self.diag("AsyncLoop.kill_server: starting")
        cmd = self.srv.get_kill_server_command()
        self.diag(f"AsyncLoop.kill_server: executing {cmd!r}")
        proc = await asyncio.create_subprocess_exec(
            *(str(word) for word in cmd),
            env=self.utf8_env,
            stdin=subprocess.DEVNULL,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
        )
        self.diag(
            f"AsyncLoop.kill_server: got process {proc.pid} for "
            f"the kill command"
        )
        rcode = await proc.wait()
        self.diag(
            f"AsyncLoop.kill_server: the kill command returned code {rcode}"
        )

        if self.proc is not None:
            self.diag(
                f"AsyncLoop.kill_server: waiting for process {self.proc.pid}"
            )
            rcode = await self.proc.wait()
            self.diag(f"AsyncLoop.kill_server: got exit code {rcode}")

    async def _schedule_pane_list_loop(self) -> None:
        """Periodically list panes to make sure we find all the dead ones."""
        while True:
            self.diag("AsyncLoop.schedule_pane_list: sleeping")
            await asyncio.sleep(5)
            self.diag("AsyncLoop.schedule_pane_list: triggering something")
            try:
                await self.msgq.put(  # type: ignore
                    SchedulePaneListEvent(esrc="schedule_pane_list")
                )
            except AttributeError:
                self.diag("AsyncLoop.schedule_pane_list: no one to notify")
                break

    async def run(self, sess_name: str, cmd: tserver.PathList) -> None:
        """Start the tmux process, attach to its streams, run the loop."""
        self.diag("AsyncLoop.run: trying to obtain the lock")
        self.session_started[sess_name] = asyncio.Queue()
        async with self.loop_lock:
            try:
                self.diag(
                    f"AsyncLoop: starting a tmux server for "
                    f"session {sess_name!r}: `{tserver.cmd_str(cmd)}`"
                )
                self.diag(
                    "AsyncLoop: config file contents:\n"
                    + self.srv.config_file.read_text(encoding="UTF-8")
                )
                proc = await self._start_server(sess_name, cmd)
                assert proc is not None
                self.proc = proc
                self.diag(
                    f"AsyncLoop: started the tmux server, pid {proc.pid}"
                )

                msgq: "asyncio.Queue[LoopEvent]" = asyncio.Queue()
                self.msgq = msgq

                break_on_stderr_t = asyncio.create_task(
                    self._break_on_stderr()
                )
                feed_server_t = asyncio.create_task(self._feed_server())
                schedule_pane_list_t = asyncio.create_task(
                    self._schedule_pane_list_loop()
                )

                try:
                    enqueued: List[LoopEvent] = []
                    enqueued_srv: List[tserver.Event] = []
                    while True:
                        if enqueued_srv:
                            for sevt in enqueued_srv:
                                enqueued.extend(
                                    await self._process_server_event(sevt)
                                )
                            enqueued_srv.clear()

                        if enqueued:
                            evt = enqueued.pop(0)
                        else:
                            evt = await msgq.get()

                        if isinstance(evt, StopLoopEvent):
                            self.diag(f"AsyncLoop: {evt!r}")
                            break

                        handlers = [
                            handler
                            for handler in [
                                get_handler(handlers, evt)
                                for handlers in [
                                    self.own_handlers,
                                    self.user_handlers,
                                ]
                            ]
                            if handler is not None
                        ]
                        if not handlers:
                            raise NotImplementedError(f"FIXME: handle {evt!r}")
                        for handler in handlers:
                            enqueued_srv.extend(await handler(evt))
                finally:
                    break_on_stderr_t.cancel()
                    feed_server_t.cancel()
                    schedule_pane_list_t.cancel()
                    self.diag(
                        "AsyncLoop: waiting for the stdout/stderr threads"
                    )
                    await asyncio.gather(
                        break_on_stderr_t,
                        feed_server_t,
                        schedule_pane_list_t,
                        return_exceptions=True,
                    )
                    self.diag("AsyncLoop: done with the stdout/stderr cleanup")
            finally:
                self.diag("AsyncLoop: clearing out our message queue")
                self.msgq = None

                self.diag("AsyncLoop: killing the tmux server")
                await self._kill_server()

    async def stop_loop(self) -> None:
        """Send a 'stop the loop' event to the message queue."""
        self.diag("AsyncLoop.stop_loop: sending the stop event")
        try:
            await self.msgq.put(  # type: ignore
                StopLoopEvent(esrc="stop_loop")
            )
        except AttributeError:
            self.diag("AsyncLoop.stop_loop: no one to send it to")
            return

        self.diag("AsyncLoop.stop_loop: trying to obtain the lock")
        async with self.loop_lock:
            self.diag("AsyncLoop.stop_loop: apparently no loop any more")

    async def send_keys_literal(self, pane_id: str, text: str) -> None:
        """Send some keys to the tmux server."""

        done = asyncio.Event()

        def cb_done() -> List[tserver.Event]:
            """The 'send keys' command was sent to the tmux server."""
            self.diag("AsyncLoop.send_keys_literal: cb_done() invoked")
            done.set()
            return []

        assert self.msgq is not None
        await self.msgq.put(
            SendLiteralEvent(
                esrc="send_keys_literal",
                pane_id=pane_id,
                text=text,
                callback=[cb_done],
            )
        )
        self.diag("AsyncLoop.send_keys_literal: waiting for the callback")
        await done.wait()
        self.diag("AsyncLoop.send_keys_literal: done")

    async def _send_keys_literal(self, evt: LoopEvent) -> List[tserver.Event]:
        """Send keys to the server."""
        assert isinstance(evt, SendLiteralEvent)
        return self.srv.send_keys_literal(
            evt.pane_id, evt.text, evt.callback[0]
        )

    async def send_key_name(self, pane_id: str, key: str) -> None:
        """Send some keys to the tmux server."""

        done = asyncio.Event()

        def cb_done() -> List[tserver.Event]:
            """The 'send keys' command was sent to the tmux server."""
            self.diag("AsyncLoop.send_key_name: cb_done() invoked")
            done.set()
            return []

        assert self.msgq is not None
        await self.msgq.put(
            SendKeyNameEvent(
                esrc="send_key_name",
                pane_id=pane_id,
                key=key,
                callback=[cb_done],
            )
        )
        self.diag("AsyncLoop.send_key_name: waiting for the callback")
        await done.wait()
        self.diag("AsyncLoop.send_key_name: done")

    async def _send_key_name(self, evt: LoopEvent) -> List[tserver.Event]:
        """Send keys to the server."""
        assert isinstance(evt, SendKeyNameEvent)
        return self.srv.send_key_name(evt.pane_id, evt.key, evt.callback[0])

    async def _send_command(self, evt: tserver.Event) -> List[LoopEvent]:
        """Send some data to the tmux server's standard input."""
        assert isinstance(evt, tserver.SendEvent)
        assert self.proc is not None and self.proc.stdin is not None
        self.diag(f"AsyncLoop._send_command: {evt.data!r}")
        self.proc.stdin.write(evt.data)
        await self.proc.stdin.drain()
        self.diag("AsyncLoop._send_command: done")
        return []

    async def _handle_pane_output(self, evt: tserver.Event) -> List[LoopEvent]:
        """Enqueue some output on a pane."""
        assert isinstance(evt, tserver.OutputEvent)
        self.diag(
            f"AsyncLoop._handle_pane_output: got {len(evt.data)} bytes "
            f"for pane {evt.pane_id}"
        )
        if not evt.data:
            return []

        await self._ensure_pane_data(evt.pane_id)
        await self.pane_data[evt.pane_id].output.put(evt.data)
        return []

    async def get_pane_output(self, pane_id: str) -> bytes:
        """Wait for a single blob of output to the specified pane."""
        await self._ensure_pane_data(pane_id)
        return await self.pane_data[pane_id].output.get()

    async def _program_ended(self, evt: tserver.Event) -> List[LoopEvent]:
        """Notify the appropriate things that a program has ended."""
        assert isinstance(evt, tserver.ProgramEndedEvent)
        await self._ensure_pane_data(evt.pane_id)
        pane_data = self.pane_data[evt.pane_id]
        await pane_data.output.put(b"")
        pane_data.rcode = evt.rcode
        pane_data.rcode_set.set()
        return []

    async def wait_for_program_end(self, pane_id: str) -> int:
        """Get the exit code of a program running in a pane."""
        await self._ensure_pane_data(pane_id)
        pane_data = self.pane_data[pane_id]
        await pane_data.rcode_set.wait()
        rcode = pane_data.rcode
        assert rcode is not None
        return rcode

    async def capture_pane(self, pane_id: str) -> List[str]:
        """Capture the contents of a window pane."""
        self.diag(f"AsyncLoop.capture_pane {pane_id}")

        capq: "asyncio.Queue[List[str]]" = asyncio.Queue()

        def cb_captured(output: List[str]) -> List[tserver.Event]:
            """The 'send keys' command was sent to the tmux server."""
            self.diag("AsyncLoop.capture_pane: cb_captured() invoked")
            return [
                SrvCapturePaneEvent(
                    evt_type="async-capture-pane", output=output, queue=capq
                )
            ]

        assert self.msgq is not None
        await self.msgq.put(
            CapturePaneEvent(
                esrc="capture_pane",
                pane_id=pane_id,
                callback=[cb_captured],
            )
        )
        self.diag("AsyncLoop.capture_pane: waiting for the callback")
        lines = await capq.get()
        self.diag(f"AsyncLoop.capture_pane: got {len(lines)} lines")
        return lines

    async def _capture_pane(self, evt: LoopEvent) -> List[tserver.Event]:
        """Send keys to the server."""
        assert isinstance(evt, CapturePaneEvent)
        return self.srv.capture_pane(evt.pane_id, evt.callback[0])

    async def _pane_captured(self, evt: tserver.Event) -> List[LoopEvent]:
        """Notify the one awaiting the captured pane contents."""
        assert isinstance(evt, SrvCapturePaneEvent)
        await evt.queue.put(evt.output)
        return []

    async def _schedule_pane_list(self, evt: LoopEvent) -> List[tserver.Event]:
        """Ask the server for a periodic pane list."""

        assert isinstance(evt, SchedulePaneListEvent)
        return self.srv.list_all_known_panes()
