"""Run a functional test or something."""

import asyncio
import asyncio.subprocess
import pathlib
import sys
import tempfile

import decode_acc.accumulator as decode_acc
import decode_acc.newlines as decode_lines
import utf8_locale

from tmux_ctlmode import async_loop
from tmux_ctlmode import server


class LoopTester:
    """Run the test using the AsyncLoop tmux loop."""

    # pylint: disable=too-few-public-methods

    loop: async_loop.AsyncLoop

    def __init__(
        self,
        config_file: pathlib.Path,
        socket_path: pathlib.Path,
    ) -> None:
        """Initialize the AsyncLoop object."""
        self.loop = async_loop.AsyncLoop(
            config_file=config_file,
            socket_path=socket_path,
            utf8_env=utf8_locale.get_utf8_env(),
            verbose=True,
        )

    async def _run_test_start(self, sess_name: str) -> str:
        """Start the program, get the empty pane's id."""
        print("run_test: waiting for the 'program started' notification.")
        evt = await self.loop.program_started(sess_name)
        print(f"run_test: got the 'program started' event: {evt!r}")
        pane_id = evt.pane_id

        print("run_test: capturing a hopefully empty pane")
        lines = await self.loop.capture_pane(pane_id)
        print(f"run_test: raw lines: {lines!r}")
        nlines = [line for line in (line.rstrip() for line in lines) if line]
        print(f"run_test: nonempty lines: {nlines!r}")
        if nlines:
            sys.exit(f"Expected an empty pane at the start, got {lines!r}")

        return pane_id

    async def _run_test_chatter(self, pane_id: str) -> None:
        """Send a line or two, get them back encrypted."""
        await self.loop.send_keys_literal(pane_id, "hello")
        print("run_test: sent the literal data")
        await self.loop.send_key_name(pane_id, "Enter")
        print("run_test: sent the 'Enter' key")
        acc = decode_acc.DecodeAccumulator()
        acc_lines: decode_lines.TextSplitter = decode_lines.UniversalNewlines()
        while True:
            print("run_test: awaiting some response from rot13")
            data = await self.loop.get_pane_output(pane_id)
            print(f"run_test: got {len(data)} bytes")
            if not data:
                sys.exit("EOF before reading the encrypted line")
            acc, decoded = acc.add(data).pop_decoded()
            if not decoded:
                print("run_test: not even a full UTF-8 character yet...")
                continue
            print(f"run_test: got {len(decoded)} characters")
            acc_lines, lines = acc_lines.add_string(decoded).pop_lines()
            if not lines:
                print("run_test: not a full line yet...")
                continue
            print(f"run_test: got {len(lines)} full lines: {lines!r}")
            indices = [
                idx for idx, line in enumerate(lines) if "uryyb" in line
            ]
            if indices:
                if len(indices) != 1 or indices[0] != len(lines) - 1:
                    sys.exit(f"Expected {len(lines) - 1}, got {indices!r}")
                break

        print("run_test: capturing a hopefully two-line pane")
        lines = await self.loop.capture_pane(pane_id)
        print(f"run_test: raw lines: {lines!r}")
        nlines = [line for line in (line.rstrip() for line in lines) if line]
        print(f"run_test: nonempty lines: {nlines!r}")
        if nlines != ["hello", "uryyb"]:
            sys.exit(f"Expected two specific lines, got {lines!r}")

    async def _run_test_shutdown(self, pane_id: str) -> None:
        """Send an EOF, wait for the program and the tmux server to stop."""
        print("run_test: sending the EOF")
        await self.loop.send_keys_literal(pane_id, bytes([4]).decode("UTF-8"))
        print("run_test: awaiting the null output")
        data = await self.loop.get_pane_output(pane_id)
        if data:
            sys.exit("Read something after the encrypted line: {data!r}")

        rcode = await self.loop.wait_for_program_end(pane_id)
        print(f"run_test: got program exit code {rcode}")
        if rcode == -1:
            print(f"Pane {pane_id} missed the pane-died notification")
        elif rcode != 0:
            sys.exit(f"The program in pane {pane_id} exited with code {rcode}")
        print("run_test: let's give the loop a chance to end")
        await asyncio.sleep(1)
        print("run_test: checking the server state")
        state = self.loop.srv.state
        if state != server.State.ENDED:
            sys.exit(f"The server is in state {state} instead of ended")
        print("run_test: done")

    async def run_test(self, sess_name: str) -> None:
        """Run the test (to be started in a task separate from the loop)."""
        pane_id = await self._run_test_start(sess_name)
        await self._run_test_chatter(pane_id)
        await self._run_test_shutdown(pane_id)

    async def run_loop(self) -> None:
        """Start the loop and the test, wait for them."""
        sess_name = "rot13-test"
        task_loop = asyncio.create_task(
            self.loop.run(
                sess_name,
                [
                    "env",
                    "LC_ALL=" + self.loop.utf8_env["LC_ALL"],
                    "LANGUAGE=" + self.loop.utf8_env["LANGUAGE"],
                    "rot13",
                ],
            )
        )
        task_test = asyncio.create_task(self.run_test(sess_name))
        try:
            done, _ = await asyncio.wait(
                [task_loop, task_test],
                timeout=30,
                return_when=asyncio.FIRST_COMPLETED,
            )
            if not done:
                sys.exit("run_test: the test timed out")
        finally:
            await self.loop.stop_loop()
            task_test.cancel()
            await asyncio.gather(task_loop, task_test)


async def main() -> None:
    """Start a server, kill it, and stuff."""
    with tempfile.TemporaryDirectory(prefix="tmux-bc.") as tempd_path:
        tempd = pathlib.Path(tempd_path)

        config_file = tempd / "tmux.conf"
        config_file.write_text(
            server.Server.get_config_file_contents(), encoding="UTF-8"
        )

        socket_path = tempd / "tmux.sock"
        print(f"Socket {socket_path}")

        tester = LoopTester(config_file, socket_path)
        await tester.run_loop()
        print("Everything seems fine!")


if __name__ == "__main__":
    asyncio.run(main())
